## 3DREAM: Three dimensional B1+ mapping

Demo code for the paper "Whole-brain B1-mapping using three dimensional DREAM" by Philipp Ehses, Daniel Brenner, Rüdiger Stirnberg, Eberhard Pracht, and Tony Stöcker.

Dependencies:
- python/ipython 2.x or 3.x
- argparse
- numpy
- matplotlib
- nibabel


Please open the file "dream_example.ipynb" for the tutorial.
